# Dipendenti

Questo programma calcola il numero di differenti matricole o i minuti di lavoro di una singola matricola.
Deve essere utilizzato da linea di comando con dei parametri che vedremo di seguito.

Per utilizzare questo programma puoi:
* usare il link [https://repl.it/@aligabbos/Dipendenti](https://repl.it/@aligabbos/Dipendenti) per provare e modificare in modo veloce il codice
* scaricare e compilare i file sorgenti di questo repository

## Parametri
### File
Qui saranno inserite le informazioni con le quali lavorerà il programma. Il formato dovrà rispettare queste regole:

H | m | matricola
--|---|----------

* **H** sono le ore nel formato 24 ore senza 0 iniziali
* **m** sono minuti nel formato senza 0 iniziali
* **matricola** stringa alfanumerica

![Formato file](img/file.png)

### Numero matricola
Servirà per calcolare i minuti lavorati dal singolo dipendente.

## Repl.it
### 1. Compilare il programma
Clicca sul tasto _**RUN**_
![Creare eseguibile](img/repl_run.png)

### 2. Eseguire il programma da shell
Clicca sulla finestra a destra ed esegui il programma usando i parametri

```bash
./main nome_file
```
oppure

```bash
./main nome_file matricola_dipendente
```

![Esegui programma da shell](img/repl_shell.png)