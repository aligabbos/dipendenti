#include "bstree.h"

Node_tp createTreeNode(char* data){
  Node_tp new_node = malloc(sizeof(Node_t));
  strcpy(new_node->data, data);
  new_node->left = NULL;
  new_node->right = NULL;
  return new_node;
}

Node_tp pushTreeNode(Node_tp root, char* data){
  if (!root)
    return createTreeNode(data);
  else if ( strcmp(data, root->data) < 0)
    root->left = pushTreeNode(root->left, data);
  else if ( strcmp(data, root->data) > 0)
    root->right = pushTreeNode(root->right, data);
  else
    return root;

  return root;
}

void popTreeNode(Node_tp* root, char* data){
  Node_tp parent, child, temp = *root;
  bool isRoot;

  //cerco il nodo genitore del nodo da eliminare
  parent = getParentTreeNode(*root, data);
  
  if(!parent)
    return;
  
  //determino se il nodo da eliminare è la root
  isRoot = strcmp(data, temp->data) == 0;
  
  //determino il nodo da eliminare
  child = isRoot? *root : ( strcmp(data, parent->data) < 0)? parent->left : parent->right;

  //il nodo da eliminare non ha figli
  if(!(child->left) && !(child->right)){
    if(isRoot){
      free(*root);
      *root = NULL;
    }else if( strcmp(child->data, parent->data) < 0)
      parent->left = NULL;
    else
      parent->right = NULL;
    
    free(child);
  }
  //il nodo da eliminare ha figli a sinistra
  else if(child->left){
    temp = child->left;

    while(temp->right)
      temp = temp->right;
    
    temp->right = child->right;

    if(isRoot)
      *root = child->left;
    else if( strcmp(child->data, parent->data) < 0)
      parent->left = child->left;
    else
      parent->right = child->left;

    free(child);
  }
  //il nodo da eliminare non ha figli a sinistra
  else{
    if(isRoot)
      *root = child->right;
    else if( strcmp(child->data, parent->data) < 0)
      parent->left = child->right;
    else
      parent->right = child->right;

    free(child);
  }
}

void deleteTree(Node_tp* root, char* root_data){
  Node_tp node = *root;

  if(!node)
    return;
  
  deleteTree(&node->left, root_data);
  deleteTree(&node->right, root_data);

  if( !strcmp(node->data, root_data) ){
      free(*root);
      *root = NULL;
  }else
    free(node);
}

bool searchTreeNode(Node_tp root, char* val){
  if(!root)
    return false;
  else if( !strcmp(val, root->data) )
    return true;
  else if( strcmp(val, root->data) < 0)
    return searchTreeNode(root->left, val);
  else
    return searchTreeNode(root->right, val);
}

Node_tp getParentTreeNode(Node_tp root, char* val){
  if(!root)
    return NULL;
  else{
    if( !strcmp(val, root->data) ){
      return root;
    } else if( (root->left && ( !strcmp(val, root->left->data) ) ) || (root->right && ( !strcmp(val, root->right->data) ) ) )
      return root;
    else if( strcmp(val, root->data) < 0)
      return getParentTreeNode(root->left, val);
    else
      return getParentTreeNode(root->right, val);
  }
}

void showInOrder(Node_tp root){
  if(!root)
    return;
  
  showInOrder(root->left);
  printf("%s ", root->data);
  showInOrder(root->right);
}

void showPreOrder(Node_tp root){
  if(!root)
    return;

  printf("%s ", root->data);
  showPreOrder(root->left);
  showPreOrder(root->right);
}

unsigned int countTreeLeaves(Node_tp root){
  int leaves = 0;

  if(!root)
    return 0;
  
  leaves += countTreeLeaves(root->left);

  if(!(root->left) && !(root->right))
    return 1;
  else
    leaves += countTreeLeaves(root->right);

  return leaves;
}

unsigned int countTreeNodes(Node_tp root){
  int nodes = 0;

  if(!root)
    return 0;
  
  nodes += countTreeNodes(root->left);

  if(!(root->left) && !(root->right))
    return 0;
  else
    nodes += ( 1 + countTreeNodes(root->right) );

  return nodes;
}

unsigned int getDimensionTree(Node_tp root){
  return countTreeLeaves(root) + countTreeNodes(root);
}