#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

//@brief Definizione del tipo struttura nodo lista
typedef struct Node_l{
  Worked_time data;
  struct Node_l* next;
}Node_l;

//@brief Definizione del tipo puntatore alla struttura nodo lista
typedef Node_l* Node_lp;

/**
  *@brief Alloca spazio per il nodo
  *@param Valore che conterrà il nodo
  *@return Puntatore del nodo creato
*/
Node_lp createListNode(Worked_time);

/**
  *@brief Inserisce i nodi in testa nella struttura dati
  *@param Root della struttura dati (riferimento) e valore che conterrà il nodo
*/
void pushListNodeHead(Node_lp*, Worked_time);

//ritorna la testa della lista
// Node_lp pushListNodeFront(Node_lp, Worked_time);

/**
  *@brief Inserisce i nodi in coda nella struttura dati
  *@param Root della struttura dati (riferimento) e valore che conterrà il nodo
  *@return Puntatore del nodo inserito (coda)
*/
Node_lp pushListNodeTail(Node_lp*, Worked_time);

/**
  *@brief Elimina un nodo in testa dalla struttura dati
  *@param Root della struttura dati (riferimento)
  *@return Valore del nodo eliminato
*/
Worked_time popListNodeHead(Node_lp*);

#endif //LIST_H