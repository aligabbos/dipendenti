#include "list.h"

Node_lp createListNode(Worked_time data){
  Node_lp newNode = malloc(sizeof(Node_l));
  newNode->data = data;
  newNode->next = NULL;
  return newNode;
}

void pushListNodeHead(Node_lp* head, Worked_time data){
  Node_lp node = createListNode(data);

  if(*head){
    node->next = *head;
    *head = node;
  }else
    *head = node;
}

Node_lp pushListNodeTail(Node_lp* head, Worked_time data){
  Node_lp node = *head;

  //lista vuota
  if(!node){
    *head = createListNode(data);
    node = *head;
  }
  else if(node->next){
    node = pushListNodeTail(&node->next, data);
  }
  else{
    node->next = createListNode(data);
    return node->next;
  }

  return node;
}

Worked_time popListNodeHead(Node_lp* head){
  Worked_time data;
  Node_lp pop_node = *head;

  *head = pop_node->next;
  data = pop_node->data;
  free(pop_node);
  return data;
}