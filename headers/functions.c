#include "functions.h"
#include "list.h"
#include "bstree.h"

FILE* readFile(char* file_name){
  FILE* fp = (FILE*) malloc(sizeof(FILE));
  fp = fopen(file_name, "r");

  if(!fp)
    perror("Error");

  return fp;
}

void minWorkDip(char* file_name, char* id_search){
  FILE* fp = readFile(file_name);
  Node_lp head = NULL;
  Worked_time dip;
  char id[10];
  time h, m;
  int i = 0, w_m = 0;

  if(fp){
    while( !feof(fp) ){
      fscanf(fp, "%d %d %s\n", &h, &m, id);

      if( !( strcmp(id_search, id) ) ){
        i += 1;

        //dispari = entrata
        if(i % 2){
          strcpy(dip.id, id);
          dip.entry.hour = h;
          dip.entry.minute = m;
          dip.working_minutes = 0;
        } else{
          dip.exit.hour = h;
          dip.exit.minute = m;

          w_m = (dip.exit.hour - dip.entry.hour) * 60;
          w_m += (dip.exit.minute - dip.entry.minute);

          dip.working_minutes = w_m;
          w_m = 0;

          pushListNodeTail(&head, dip);
        }
      }
    }

    if(!i){
      printf("Codice dipendente non trovato\n");
    } else{
        if(i % 2){
          printf("Attenzione: uscita non registrata\n");
          printf("Ultima entrata registrata: %d.%d\n", dip.entry.hour, dip.entry.minute);
        }

        while(head){
          dip = popListNodeHead(&head);
          w_m += dip.working_minutes;
        }

        printf("Il dipendente (matricola %s) ha lavorato per %d minuti\n", dip.id, w_m);
    }

    fclose(fp);
  }
}

void numDip(char* file_name){
  FILE* fp = readFile(file_name);
  Node_tp root = NULL;
  char id[10];
  time h, m;
  int n_dip = 0;

  if(fp){
    while( !feof(fp) ){
      fscanf(fp, "%d %d %s\n", &h, &m, id);
      
      if(*id)
        root = pushTreeNode(root, id);
    }

    n_dip = getDimensionTree(root);

    if(n_dip)
      printf("Ci sono %d dipendenti diversi\n", n_dip);
    else
      printf("File vuoto\n");

    deleteTree(&root, root->data);
    fclose(fp);
  }
}