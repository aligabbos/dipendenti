#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <stdbool.h>

//@brief Definizione del tipo struttura nodo albero
typedef struct Node_t{
  char data[10];
  struct Node_t *left, *right;
}Node_t;

//@brief Definizione del tipo puntatore alla struttura nodo albero
typedef Node_t* Node_tp;

/**
  *@brief Alloca spazio per il nodo
  *@param Valore che conterrà il nodo
  *@return Puntatore del nodo creato
*/
Node_tp createTreeNode(char*);

/**
  *@brief Inserisce i nodi nella struttura dati
  *@param Root della struttura dati e valore che conterrà il nodo
  *@return Puntatore del nodo inserito
*/
Node_tp pushTreeNode(Node_tp, char*);

/**
  *@brief Elimina un nodo dalla struttura dati
  *@param Root della struttura dati (riferimento) e valore da eliminare
*/
void popTreeNode(Node_tp*, char*);

/**
  *@brief Elimina tutta la struttura dati
  *@param Root della struttura dati (riferimento) e valore della root
*/
void deleteTree(Node_tp*, char*);

/**
  *@brief Cerca i valori nella struttura dati
  *@param Root della struttura dati e valore da cercare
  *@return Valore booleano
*/
bool searchTreeNode(Node_tp, char*);

/**
  *@brief Cerca il nodo genitore
  *@param Root della struttura dati e valore da cercare
  *@return Puntatore del nodo genitore rispetto al valore cercato
*/
Node_tp getParentTreeNode(Node_tp, char*);

/**
  *@brief Stampa i valori della struttura con metodo di attraversamento SRD
  *@param Root della struttura dati
*/
void showInOrder(Node_tp);

/**
  *@brief Stampa i valori della struttura con metodo di attraversamento RSD
  *@param Root della struttura dati
*/
void showPreOrder(Node_tp);

/**
  *@brief Conta il numero di foglie
  *@param Root della struttura dati
  *@return Numero di foglie
*/
unsigned int countTreeLeaves(Node_tp);

/**
  *@brief Conta il numero di nodi
  *@param Root della struttura dati
  *@return Numero di nodi
*/
unsigned int countTreeNodes(Node_tp);

/**
  *@brief Conta il numero di elementi
  *@param Root della struttura dati
  *@return Numero di elementi
*/
unsigned int getDimensionTree(Node_tp);

#endif //BINARY_SEARCH_TREE_H