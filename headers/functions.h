#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef unsigned int time;

typedef struct{
  time hour;
  time minute;
} Orario;

typedef struct{
  char id[10];
  Orario entry;
  Orario exit;
  time working_minutes;
} Worked_time;

//restituisce il puntatore al file aperto
//ATTENZIONE: non chiude il file
FILE* readFile(char*);
void minWorkDip(char*, char*);
void numDip(char*);

#endif //FUNCTIONS_H